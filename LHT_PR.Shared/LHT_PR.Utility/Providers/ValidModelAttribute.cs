﻿using LHT_PR.Shared.Base.Shared.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Providers
{
    public class ValidModelAttribute : ActionFilterAttribute
    {
        public ValidModelAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var valid = ExcecutingAsync(actionContext).Result;
            if (valid)
                base.OnActionExecuting(actionContext);
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var valid = await ExcecutingAsync(context);
            if (valid)
                await base.OnActionExecutionAsync(context, next);
        }

        private async Task<bool> ExcecutingAsync(ActionExecutingContext actionContext)
        {
            if (!actionContext.ModelState.IsValid || actionContext.ActionArguments.Any(kv => kv.Value == null))
            {
                var response = new HttpResponse<object>
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Data = null
                };

                var msgData = actionContext.ModelState
                        .Where(ms => ms.Value.Errors.Any())
                        .Select(m => new
                        {
                            key = m.Key,
                            value = m.Value.Errors.FirstOrDefault().ErrorMessage
                        })
                        .ToList();

                response.Message = JsonSerializer.Serialize(msgData);
                var responseContext = actionContext.HttpContext.Response;
                responseContext.StatusCode = response.StatusCode;
                responseContext.ContentType = "application/json";
                await responseContext.Body.WriteAsync(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(response)));
                return false;
            }

            return true;
        }
    }
}