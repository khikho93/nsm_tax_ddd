﻿using LHT_PR.Shared.Base.DTOs.Storage;
using LHT_PR.Shared.Base.Interfaces.Storage;
using LHT_PR.Shared.Utility.Http.HttpFile;
using LHT_PR.Shared.Utility.Storage;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Shared.Utility.Services
{
    public class HttpFileService : IHttpFileService
    {
        private readonly FileServiceSettings _settings;

        public HttpFileService(IConfiguration configuration)
        {
            _settings = configuration.GetSection(ConfigKeys.STORAGE_SETTINGS).Get<FileServiceSettings>();
        }

        public async Task<IFileResult> DeleteAsync(string fileName, bool isSecure = true)
        {
            using (BaseHttpFile poster = new HttpDeleteFile())
            {
                poster.AddCredential(_settings.User, _settings.Password);
                string fileUri = GetFileUri(string.Empty, isSecure);

                var response = await poster.SendAsync(null, fileName, fileUri);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return FileResult.OK();
                }
                else
                    return FileResult.Error("");
            }
        }

        public async Task<IDownloadFileResult> DownloadAsync(string fileName, bool isSecure = true)
        {
            using (BaseHttpFile poster = new HttpGetFile())
            {
                poster.AddCredential(_settings.User, _settings.Password);
                string fileUri = GetFileUri(fileName, isSecure);

                var response = await poster.SendAsync(null, fileName, fileUri);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var fileData = await response.Content.ReadAsByteArrayAsync();
                    return DownloadFileResult.OK(fileData);
                }
                else
                    return DownloadFileResult.Error("");
            }
        }

        public Task<IFileResult> UpdateAsync(string fileName, byte[] fileData, bool isSecure = true)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IFileResult> UploadAsync(string fileName, byte[] fileData, bool isSecure = true)
        {
            using (BaseHttpFile poster = new HttpPostFile())
            {
                poster.AddCredential(_settings.User, _settings.Password);
                string fileUri = GetFileUri(string.Empty, isSecure);

                var response = await poster.SendAsync(fileData, fileName, fileUri);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return FileResult.OK();
                }
                else
                    return FileResult.Error("");
            }
        }

        public string GetFileUri(string fileName, bool isSecure)
        {
            string host = $"{_settings.ServerName}";
            if (!string.IsNullOrEmpty(_settings.Port))
                host += $":{ _settings.Port}";
            return string.Join("/", host, (isSecure ? _settings.PrivateFolder : _settings.PublicFolder), fileName);
        }

        public string GetFolderUri(bool isSecure)
        {
            string host = $"{_settings.ServerName}";
            if (!string.IsNullOrEmpty(_settings.Port))
                host += $":{ _settings.Port}";
            return string.Join("/", host, (isSecure ? _settings.PrivateFolder : _settings.PublicFolder)) + "/";
        }
    }
}