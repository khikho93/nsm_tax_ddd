﻿using LHT_PR.Shared.Base.DTOs.Mail;
using LHT_PR.Shared.Base.Interfaces.Helper;
using LHT_PR.Shared.Base.Interfaces.Logging;
using LHT_PR.Shared.Base.Services;
using System;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Services
{
    public class EmailService : IEmailService
    {
        private readonly IMailHelper        _mailHelper;
        private readonly IAppLogger         _appLogger;
        private readonly IMailBodyGenerator _mailBodyGenerator;

        public delegate Task SendMailHandler(string[] toEmails, string subject, string bodyTemplate, MailComplieModel model);

        public EmailService(IMailHelper mailHelper
            , IMailBodyGenerator mailBodyGenerator
            , IAppLogger appLogger
            )
        {
            _appLogger         = appLogger;
            _mailBodyGenerator = mailBodyGenerator;
            _mailHelper        = mailHelper;
        }

        public void Send(string[] toEmails, string subject, string bodyTemplate, MailComplieModel model)
        {
            var handler = new SendMailHandler(SendMailBackground);
            Task.Run(() =>
            {
                handler(toEmails, subject, bodyTemplate, model);
            });
        }

        private async Task SendMailBackground(string[] toEmails
            , string subject
            , string bodyTemplate
            , MailComplieModel model)
        {
            try
            {
                var body = await _mailBodyGenerator.GenerateBodyAsync(bodyTemplate, model);
                _mailHelper.Send(toEmails, subject, body);
            }
            catch (Exception ex)
            {
                _appLogger.Error(ex);
            }
        }
    }
}