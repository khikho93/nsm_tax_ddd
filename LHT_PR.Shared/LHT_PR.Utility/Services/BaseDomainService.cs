﻿using LHT_PR.Shared.Base.Entities;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Http.HttpHandlers;
using LHT_PR.Shared.Utility.Http.HttpRequests;
using MediatR;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Services
{
    public abstract class BaseDomainService
    {
        protected readonly IMediator Mediator;

        public BaseDomainService()
        {
            Mediator = HttpAppContext.GetRequestService<IMediator>();
        }
    }

    public class BaseDomainService<T> : BaseDomainService
        where T : EntityBase
    {
        /// <summary>
        /// create entity async
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual Task<HttpResponse<int>> CreateAsync(CreateRequest<T> request)
        {
            var handler = new CreateHandler<T>();
            return handler.Handle(request, default);
        }

        /// <summary>
        /// Update entity async
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual Task<HttpResponse<int>> UpdateAsync(UpdateRequest<T> request)
        {
            var handler = new UpdateHandler<T>();
            return handler.Handle(request, default);
        }

        /// <summary>
        /// Delete entity async
        /// </summary>
        /// <param name="entityModel"></param>
        /// <returns></returns>
        public virtual Task<HttpResponse<int>> DeleteAsync(DeleteRequest<T> request)
        {
            var handler = new DeleteHandler<T>();
            return handler.Handle(request, default);
        }

        /// <summary>
        /// Find entity async
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual Task<HttpResponse<T>> FindAsync(FindRequest<T> request)
        {
            var handler = new FindHandler<T>();
            return handler.Handle(request, default);
        }
    }
}