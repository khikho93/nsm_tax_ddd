﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Shared.Utility.Identity.Policies
{
    public class UserTypePolicyHandler : AuthorizationHandler<UserTypePolicyRequirement>
    {
        public UserTypePolicyHandler()
        {
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserTypePolicyRequirement requirement)
        {
            // Check user type to allow access to Microservice
            if (context.User.HasClaim(c => c.Type == AppClaimTypes.USER_TYPE && requirement.UserTypes.Contains(c.Value)))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}