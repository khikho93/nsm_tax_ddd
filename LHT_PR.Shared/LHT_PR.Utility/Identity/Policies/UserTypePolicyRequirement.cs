﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace LHT_PR.Shared.Utility.Identity.Policies
{
    public class UserTypePolicyRequirement : IAuthorizationRequirement
    {
        public UserTypePolicyRequirement(params string[] userTypes)
        {
            this.UserTypes = userTypes;
        }

        public string[] UserTypes { get; set; }
    }
}