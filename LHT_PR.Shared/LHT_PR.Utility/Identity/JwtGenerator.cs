﻿using LHT_PR.Shared.Base.Identity;
using LHT_PR.Shared.Base.Interfaces.Identity;
using LHT_PR.Shared.Base.Shared;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LHT_PR.Shared.Utility.Identity
{
    public class JwtGenerator : IJwtGenerator
    {
        public string Generate(JwtTokenSettings settings, params Claim[] claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(settings.AccessExpiration));

            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var token = new JwtSecurityToken(
                issuer: settings.Issuer,
                audience: settings.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: expires,
                signingCredentials: creds
                );

            return jwtTokenHandler.WriteToken(token);
        }
    }
}