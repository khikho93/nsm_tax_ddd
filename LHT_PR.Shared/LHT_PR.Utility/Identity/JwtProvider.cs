﻿using LHT_PR.Shared.Base.Identity;
using LHT_PR.Shared.Base.Interfaces.Identity;
using LHT_PR.Shared.Utility.Resources;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Shared.Utility.Identity
{
    public class JwtProvider : IJwtProvider
    {
        private readonly IConfiguration _configuration;
        private readonly IJwtGenerator _jwtGenerator;

        public JwtProvider(IConfiguration configuration
            , IJwtGenerator jwtGenerator
            )
        {
            _configuration = configuration;
            _jwtGenerator = jwtGenerator;
        }

        public string GenerateJwtToken(Claim[] claims)
        {
            if (claims != null && claims.Length > 0)
            {
                var tokenSettings = _configuration.GetSection(ConfigKeys.JWT_TOKEN).Get<JwtTokenSettings>();

                return _jwtGenerator.Generate(tokenSettings, claims.ToArray());
            }
            else
                return string.Empty;
        }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public bool ValidateAudience(string issuer
            , string secretKey
            , ref string errorMessage)
        {
            var tokenSettings = _configuration.GetSection(ConfigKeys.JWT_TOKEN).Get<JwtTokenSettings>();

            if (tokenSettings.Issuer != issuer)
            {
                errorMessage = string.Format(ErrorMessages.Login_Not_Permitted, nameof(tokenSettings.Issuer));
                return false;
            }

            if (tokenSettings.SecretKey != secretKey)
            {
                errorMessage = string.Format(ErrorMessages.Login_Not_Permitted, nameof(tokenSettings.SecretKey));
                return false;
            }

            return true;
        }
    }
}