﻿using LHT_PR.Shared.Base.DTOs.Mail;
using LHT_PR.Shared.Base.Interfaces.Helper;
using LHT_PR.Shared.Base.Interfaces.Logging;
using Microsoft.Extensions.Configuration;
using System;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Shared.Utility.Helpers
{
    public class MailHelper : IMailHelper
    {
        private readonly IConfiguration _configuration;
        private readonly SmtpClient     _smtpClient;
        private readonly SmtpSettings   _settings;

        public delegate void SendMailHandler(string[] toEmails, string subject, string body, object attachs = null);

        public MailHelper(IConfiguration configuration
            , IAppLogger appLogger)
        {
            _configuration = configuration;

            _settings = _configuration.GetSection(ConfigKeys.SMTP_SETTINGS).Get<SmtpSettings>();

            _smtpClient = new SmtpClient
            {
                Host           = _settings.Host,
                EnableSsl      = false,
                Port           = Convert.ToInt32(_settings.Port),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials    = new NetworkCredential(_settings.Account, _settings.Password),
                Timeout        = 30000
            };
        }

        public void Send(string[] toEmails, string subject, string body)
        {
            Send(toEmails, subject, body, null);
        }

        private void Send(string[] toEmails, string subject, string body, object attachs = null)
        {
            var fromAddress = new MailAddress(_settings.Account, "ITR Server");

            MailMessage message = new MailMessage()
            {
                From       = fromAddress,
                Subject    = subject,
                //Body       = body,
                IsBodyHtml = true
            };

            // Create the HTML view
            var htmlView = AlternateView.CreateAlternateViewFromString(body,
                                                         Encoding.UTF8,
                                                         MediaTypeNames.Text.Html);
            var imageLinked = EmailLogoLinkedResource();
            htmlView.LinkedResources.Add(imageLinked);
            message.AlternateViews.Add(htmlView);


            foreach (var toEmail in toEmails)
            {
                message.To.Add(toEmail);
            }

            _smtpClient.Send(message);
        }

        private LinkedResource EmailLogoLinkedResource()
        {
            var assembly                   = Assembly.GetAssembly(typeof(MailHelper));
            var imageStream                = assembly.GetManifestResourceStream("ITR.Shared.Utility.Resources.Files.logo.png");

            var resource                   = new LinkedResource(imageStream);
            resource.ContentId             = "logo_embedded";
            resource.ContentType.MediaType = MediaTypeNames.Image.Jpeg;
            resource.TransferEncoding      = TransferEncoding.Base64;
            resource.ContentType.Name      = resource.ContentId;
            resource.ContentLink           = new Uri("cid:" + resource.ContentId);

            return resource;
        }
    }
}