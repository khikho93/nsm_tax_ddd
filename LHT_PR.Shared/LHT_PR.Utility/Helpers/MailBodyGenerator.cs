﻿using LHT_PR.Shared.Base.DTOs.Mail;
using LHT_PR.Shared.Base.Interfaces.Helper;
using RazorLight;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Helpers
{
    public class MailBodyGenerator : IMailBodyGenerator
    {
        public Task<string> GenerateBodyAsync(string html, MailComplieModel model)
        {
            var engine = new RazorLightEngineBuilder()
                .UseEmbeddedResourcesProject(Assembly.GetExecutingAssembly())
                .UseMemoryCachingProvider()
                .Build();

            return engine.CompileRenderStringAsync(Guid.NewGuid().ToString(), html, model);
        }
    }
}