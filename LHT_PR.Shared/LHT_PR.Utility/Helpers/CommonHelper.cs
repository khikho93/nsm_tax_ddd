﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LHT_PR.Shared.Utility.Helpers
{
    public static class CommonHelper
    {
        /// <summary>
        /// Convert object data
        /// </summary>
        /// <typeparam name="T">New object type</typeparam>
        /// <param name="fromObject">Object contain data</param>
        /// <param name="newObject">Object will be filled data</param>
        public static T ConvertTo<T>(this object fromObject)
        {
            var newObject = Activator.CreateInstance<T>();
            if (fromObject == null)
            {
                return newObject;
            }

            Type type = fromObject.GetType();
            Type typeT = newObject.GetType();

            foreach (var f in type.GetProperties())
            {
                object val = f.GetValue(fromObject, null);

                var pt = typeT.GetProperty(f.Name);
                if (pt != null)
                {
                    if (pt.PropertyType == f.PropertyType)
                    {
                        pt.SetValue(newObject, val, null);
                    }
                }
            }
            return newObject;
        }
    }
}
