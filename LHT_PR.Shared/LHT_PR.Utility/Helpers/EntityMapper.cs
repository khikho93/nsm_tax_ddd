﻿using LHT_PR.Shared.Base.Interfaces.Entities;
using LHT_PR.Shared.Base.Shared.Http;
using System;

namespace LHT_PR.Shared.Utility.Helpers
{
    public static class EntityMapper
    {
        public static IEntity ToNewLogEntity(IAuditEntity entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.CreatedBy = HttpAppContext.Current.User.Identity.Name;
            entity.ModifiedBy = HttpAppContext.Current.User.Identity.Name;
            entity.ModifiedDate = DateTime.UtcNow;
            return entity;
        }
        public static IEntity ToUpdateLogEntity(IAuditEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            entity.ModifiedBy = HttpAppContext.Current.User.Identity.Name;
            return entity;
        }
        public static IEntity ToDeleteLogEntity(IDeleteEntity entity, bool isDelete = true)
        {
            entity.IsDeleted = isDelete;
            return entity;
        }
    }
}
