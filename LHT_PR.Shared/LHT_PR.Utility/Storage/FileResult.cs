﻿using LHT_PR.Shared.Base.Interfaces.Storage;

namespace LHT_PR.Shared.Utility.Storage
{
    public class FileResult : IFileResult
    {
        public FileResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public bool Success { get; private set; }

        public string Message { get; private set; }

        public static IFileResult OK(string message = null)
        {
            return new FileResult(true, message);
        }

        public static IFileResult Error(string errorMessage)
        {
            return new FileResult(false, errorMessage);
        }
    }
}