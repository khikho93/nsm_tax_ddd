﻿using LHT_PR.Shared.Base.Interfaces.Storage;

namespace LHT_PR.Shared.Utility.Storage
{
    public class DownloadFileResult : FileResult, IDownloadFileResult
    {
        public byte[] Data { get; private set; }

        public DownloadFileResult(bool success, string message, byte[] data)
            : base(success, message)
        {
            Data = data;
        }

        public static DownloadFileResult OK(byte[] data, string message = null)
        {
            return new DownloadFileResult(true, message, data);
        }

        public static DownloadFileResult Error(string errorMessage)
        {
            return new DownloadFileResult(false, errorMessage, null);
        }
    }
}