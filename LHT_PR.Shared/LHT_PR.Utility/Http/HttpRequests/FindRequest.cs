﻿using LHT_PR.Shared.Base.Shared.Http;
using MediatR;
using System;

namespace LHT_PR.Shared.Utility.Http.HttpRequests
{
    public class FindRequest<T> : IRequest<HttpResponse<T>> where T : class
    {
        public Guid Id { get; set; }
    }
}