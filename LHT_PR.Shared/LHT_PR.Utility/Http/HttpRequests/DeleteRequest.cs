﻿using LHT_PR.Shared.Base.Shared.Http;
using MediatR;
using System;

namespace LHT_PR.Shared.Utility.Http.HttpRequests
{
    public class DeleteRequest<T> : IRequest<HttpResponse<int>>
    {
        public Guid Id { get; set; }
    }
}