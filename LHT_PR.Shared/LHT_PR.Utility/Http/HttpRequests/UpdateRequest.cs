﻿using LHT_PR.Shared.Base.Shared.Http;
using MediatR;

namespace LHT_PR.Shared.Utility.Http.HttpRequests
{
    public class UpdateRequest<T> : IRequest<HttpResponse<int>>
    {
        public T Entity { get; set; }
    }
}