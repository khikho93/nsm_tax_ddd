﻿using LHT_PR.Shared.Base.Shared.Http;
using MediatR;

namespace LHT_PR.Shared.Utility.Http.HttpRequests
{
    public class CreateRequest<T> : IRequest<HttpResponse<int>> where T : class
    {
        public T Entity { get; set; }
    }
}