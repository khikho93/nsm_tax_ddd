﻿using LHT_PR.Shared.Base.Entities;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Http.HttpRequests;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Http.HttpHandlers
{
    public class CreateHandler<T>
        : BaseHandler
        , IRequestHandler<CreateRequest<T>, HttpResponse<int>> where T : EntityBase
    {
        private readonly IRepositoryBase<T> _repository;

        public CreateHandler()
        {
            _repository = HttpAppContext.GetRequestService<IRepositoryBase<T>>();
        }

        public async Task<HttpResponse<int>> Handle(CreateRequest<T> request, CancellationToken cancellationToken)
        {
            _repository.Insert(request.Entity);
            var saved = await UnitOfWork.CommitAsync();
            if (saved > 0)
                return HttpResponse<int>.OK(saved);
            else
                return HttpResponse<int>.Error(statusCode: System.Net.HttpStatusCode.NotModified);
        }
    }
}