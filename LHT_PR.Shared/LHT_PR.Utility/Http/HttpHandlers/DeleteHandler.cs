﻿using LHT_PR.Shared.Base.Entities;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Http.HttpRequests;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Http.HttpHandlers
{
    public class DeleteHandler<T>
        : BaseHandler
        , IRequestHandler<DeleteRequest<T>, HttpResponse<int>> where T : EntityBase
    {
        private readonly IRepositoryBase<T> _repository;

        public DeleteHandler()
        {
            //_repository = repository;
        }

        public async Task<HttpResponse<int>> Handle(DeleteRequest<T> request, CancellationToken cancellationToken)
        {
            var deleteItem = await _repository.FindAsync(e => e.Id == request.Id);
            if (deleteItem == null)
                return HttpResponse<int>.Error(statusCode: System.Net.HttpStatusCode.NoContent);
            else
                return HttpResponse<int>.OK(await UnitOfWork.CommitAsync());
        }
    }
}