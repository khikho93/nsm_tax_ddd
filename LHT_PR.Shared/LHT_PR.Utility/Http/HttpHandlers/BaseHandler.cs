﻿using AutoMapper;
using LHT_PR.Shared.Base.Interfaces.Database;
using LHT_PR.Shared.Base.Interfaces.Logging;
using LHT_PR.Shared.Base.Shared.Http;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace LHT_PR.Shared.Utility.Http.HttpHandlers
{
    public abstract class BaseHandler
    {
        readonly protected IUnitOfWork UnitOfWork;
        readonly protected IMapper Mapper;
        readonly protected IAppLogger AppLogger;

        protected Identity CurrentUser
        {
            get
            {
                return new Identity(HttpAppContext.Current.User);
            }
        }

        
        public BaseHandler()
        {
            UnitOfWork = HttpAppContext.GetRequestService<IUnitOfWork>();
            Mapper = HttpAppContext.GetRequestService<IMapper>();
            AppLogger = HttpAppContext.GetRequestService<IAppLogger>();
        }
    }
    public class Identity : IIdentity
    {
        public Identity(ClaimsPrincipal identity)
        {
            AuthenticationType = identity.Identity.AuthenticationType;
            IsAuthenticated = identity.Identity.IsAuthenticated;
            Name = identity.Identity.Name;
            long id;
            if(long.TryParse(identity.Claims.FirstOrDefault(c=>c.Type== ClaimTypes.NameIdentifier)?.Value, out id))
            {
                Id = id;
            }
        }
        public string AuthenticationType { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Name { get; set; }
        public long? Id { get; set; }
    }
}