﻿using LHT_PR.Shared.Base.Entities;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Http.HttpRequests;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Http.HttpHandlers
{
    public class FindHandler<T>
        : BaseHandler
        , IRequestHandler<FindRequest<T>, HttpResponse<T>> where T : EntityBase
    {
        private readonly IRepositoryBase<T> _repository;

        public FindHandler()
        {
            _repository = HttpAppContext.GetRequestService<IRepositoryBase<T>>();
        }

        public async Task<HttpResponse<T>> Handle(FindRequest<T> request, CancellationToken cancellationToken)
        {
            var found = await _repository.FindAsync(e => e.Id == request.Id);
            if (found != null)
                return HttpResponse<T>.OK(found);
            else
                return HttpResponse<T>.Error(statusCode: System.Net.HttpStatusCode.NotFound);
        }
    }
}