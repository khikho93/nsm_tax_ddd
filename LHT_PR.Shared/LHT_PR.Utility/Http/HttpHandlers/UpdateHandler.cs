﻿using LHT_PR.Shared.Base.Entities;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Http.HttpRequests;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Utility.Http.HttpHandlers
{
    public class UpdateHandler<T>
        : BaseHandler
        , IRequestHandler<UpdateRequest<T>, HttpResponse<int>> where T : EntityBase
    {
        private readonly IRepositoryBase<T> _repository;

        public UpdateHandler()
        {
            _repository = HttpAppContext.GetRequestService<IRepositoryBase<T>>();
        }

        public async Task<HttpResponse<int>> Handle(UpdateRequest<T> request, CancellationToken cancellationToken)
        {
            var updateEntity = await _repository.FindAsync(e => e.Id == request.Entity.Id);

            if (updateEntity == null)
                return HttpResponse<int>.Error(statusCode: System.Net.HttpStatusCode.NoContent);

            CloneEntity(request.Entity, ref updateEntity);

            var saved = await UnitOfWork.CommitAsync();
            if (saved > 0)
                return HttpResponse<int>.OK(saved);
            else
                return HttpResponse<int>.Error(statusCode: System.Net.HttpStatusCode.NotModified);
        }

        private void CloneEntity(T fromEntity, ref T toEntity)
        {
            string IdName = nameof(fromEntity.Id);
            var props = typeof(T).GetProperties().Where(e => e.CanWrite && e.Name != IdName);

            foreach (var prop in props)
            {
                prop.SetValue(toEntity, prop.GetValue(fromEntity));
            }
        }
    }
}