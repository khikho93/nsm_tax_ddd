﻿namespace LHT_PR.Shared.Base.Identity
{
    public class JwtTokenSettings
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string AccessExpiration { get; set; }
        public string RefreshExpiration { get; set; }
    }
}