﻿namespace LHT_PR.Shared.Base.Identity
{
    public class LoginResult
    {
        public string JwtToken { get; set; }
        public string RefreshToken { get; set; }
    }
}