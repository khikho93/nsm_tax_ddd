﻿namespace LHT_PR.Shared.Base.Identity
{
    public class LoginUserProfile : LoginResult
    {
        public string Icon { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string UserType { get; set; }
    }
}