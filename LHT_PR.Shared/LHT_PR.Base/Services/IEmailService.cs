﻿using LHT_PR.Shared.Base.DTOs.Mail;

namespace LHT_PR.Shared.Base.Services
{
    public interface IEmailService
    {
        void Send(string[] toEmails, string subject, string bodyTemplate, MailComplieModel model);
    }
}