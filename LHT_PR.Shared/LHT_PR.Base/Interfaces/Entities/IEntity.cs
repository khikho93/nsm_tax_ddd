﻿using System;

namespace LHT_PR.Shared.Base.Interfaces.Entities
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}