﻿using System;

namespace LHT_PR.Shared.Base.Interfaces.Entities
{
    public interface IAuditEntity : IEntity
    {
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedDate { get; set; }
    }
}