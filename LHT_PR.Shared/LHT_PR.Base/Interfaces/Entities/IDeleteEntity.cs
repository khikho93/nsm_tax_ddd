﻿namespace LHT_PR.Shared.Base.Interfaces.Entities
{
    public interface IDeleteEntity : IEntity
    {
        bool IsDeleted { get; set; }
    }
}