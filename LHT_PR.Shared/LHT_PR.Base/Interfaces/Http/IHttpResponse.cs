﻿namespace LHT_PR.Shared.Base.Interfaces.Http
{
    public interface IHttpResponse
    {
        int StatusCode { get; set; }
        string Message { get; set; }
    }

    public interface IHttpResponse<T> : IHttpResponse
    {
        T Data { get; set; }
    }
}