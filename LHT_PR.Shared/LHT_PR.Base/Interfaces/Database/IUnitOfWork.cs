﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Interfaces.Database
{
    public interface IUnitOfWork
    {
        int Commit();

        Task<int> CommitAsync();

        DbContext CreateDbContext();
    }
}