﻿using Microsoft.EntityFrameworkCore;

namespace LHT_PR.Shared.Base.Interfaces.Database
{
    public interface IDbFactory
    {
        DbContext DbContext { get; set; }
    }
}