﻿using LHT_PR.Shared.Base.Identity;
using LHT_PR.Shared.Base.Shared;
using System.Security.Claims;

namespace LHT_PR.Shared.Base.Interfaces.Identity
{
    public interface IJwtGenerator
    {
        /// <summary>
        /// Generate the Jwt token with settings and claims information
        /// </summary>
        /// <param name="settings">JWT token settings</param>
        /// <param name="claims">Claims information</param>
        /// <returns>JWT token string</returns>
        string Generate(JwtTokenSettings settings, params Claim[] claims);
    }
}