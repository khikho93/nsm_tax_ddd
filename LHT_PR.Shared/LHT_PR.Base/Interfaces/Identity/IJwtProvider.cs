﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Interfaces.Identity
{
    public interface IJwtProvider
    {
        string GenerateJwtToken(Claim[] claims);

        bool ValidateAudience(string issuer
            , string secretKey
            , ref string errorMessage);

        string GenerateRefreshToken();
    }
}