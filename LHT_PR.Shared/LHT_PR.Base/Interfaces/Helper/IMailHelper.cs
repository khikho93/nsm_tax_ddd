﻿namespace LHT_PR.Shared.Base.Interfaces.Helper
{
    public interface IMailHelper
    {
        void Send(string[] toEmails, string subject, string body);
    }
}