﻿namespace LHT_PR.Shared.Base.Interfaces.Helper
{
    public interface IExcelHelper
    {
        byte[] Export<T>(T model);
    }
}