﻿using OfficeOpenXml;
using System.Collections.Generic;
using LHT_PR.Shared.Base.Attributes.Office;

namespace LHT_PR.Shared.Base.Interfaces.Helper
{
    public interface IExcelFormatter
    {
        void FormatRange(ExcelColAttribute formatInfo, ExcelRange range);

        void SetWidth(List<ExcelHeaderColAttribute> headersAttributes, ExcelWorksheet sheet);
    }
}