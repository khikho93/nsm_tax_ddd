﻿using OfficeOpenXml;

namespace LHT_PR.Shared.Base.Interfaces.Helper
{
    public interface IExcelGenerator
    {
        ExcelPackage Generate<T>(T model);
    }
}