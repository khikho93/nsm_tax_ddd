﻿using LHT_PR.Shared.Base.DTOs.Mail;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Interfaces.Helper
{
    public interface IMailBodyGenerator
    {
        Task<string> GenerateBodyAsync(string html, MailComplieModel model);
    }
}