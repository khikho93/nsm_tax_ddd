﻿namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IEntityConvert<TEntity>
    {
        TEntity GetEntity<TSource>(TSource source);

        TEntity GetEntity();

        TEntity GetEntity(TEntity source);
    }
}