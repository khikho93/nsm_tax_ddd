﻿namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IOrderable
    {
        int Order { get; set; }
    }
}