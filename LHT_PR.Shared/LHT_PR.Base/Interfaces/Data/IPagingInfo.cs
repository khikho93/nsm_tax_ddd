﻿namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IPagingInfo
    {
        int PageNo { get; set; }

        int PageSize { get; set; }
    }
}