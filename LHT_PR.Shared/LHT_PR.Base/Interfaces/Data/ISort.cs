﻿namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface ISort
    {
        string Propety { get; set; }
        bool IsDesc { get; set; }
    }
}