﻿using System;
using System.Linq.Expressions;

namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IFilterExpression<T>
    {
        Expression<Func<T, bool>> GetExpression();
    }
}