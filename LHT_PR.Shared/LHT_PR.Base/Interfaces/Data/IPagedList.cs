﻿namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IPagedList
    {
        int PageSize { get; set; }
        long TotalRecords { get; set; }
        int TotalPage { get; }
        int PageNo { get; set; }
    }
}