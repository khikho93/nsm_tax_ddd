﻿using System;
using System.Linq.Expressions;

namespace LHT_PR.Shared.Base.Interfaces.Data
{
    public interface IQuerySelection<TIn, TOut>
    {
        Expression<Func<TIn, TOut>> GetSelection();
    }
}