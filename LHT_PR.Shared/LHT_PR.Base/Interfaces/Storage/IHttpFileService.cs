﻿using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Interfaces.Storage
{
    public interface IHttpFileService
    {
        /// <summary>
        /// Upload file to Http File server
        /// </summary>
        /// <param name="fileName">File name will save on Http Server</param>
        /// <param name="fileData">File data will save on Http Server</param>
        /// <param name="isSecure">private or public</param>
        /// <returns></returns>
        Task<IFileResult> UploadAsync(string fileName, byte[] fileData, bool isSecure = true);

        /// <summary>
        /// Download file from Http File server
        /// </summary>
        /// <param name="fileName">File name will save on Http Server</param>
        /// <param name="isSecure">private or public</param>
        /// <returns></returns>
        Task<IDownloadFileResult> DownloadAsync(string fileName, bool isSecure = true);

        /// <summary>
        /// Delete file from Http File server
        /// </summary>
        /// <param name="fileName">File name will save on Http Server</param>
        /// <param name="isSecure">private or public</param>
        /// <returns></returns>
        Task<IFileResult> DeleteAsync(string fileName, bool isSecure = true);

        /// <summary>
        /// Update file to Http File server
        /// </summary>
        /// <param name="fileName">File name will save on Http Server</param>
        /// <param name="fileData">File data will save on Http Server</param>
        /// <param name="isSecure">private or public</param>
        /// <returns></returns>
        Task<IFileResult> UpdateAsync(string fileName, byte[] fileData, bool isSecure = true);

        /// <summary>
        /// Get Full URI of file on Storage Server
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="isSecure">Is public or private file</param>
        /// <returns></returns>

        string GetFileUri(string fileName, bool isSecure);

        /// <summary>
        /// Get URI of folder on Storage Server
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="isSecure">Is public or private file</param>
        /// <returns></returns>

        string GetFolderUri(bool isSecure);
    }
}