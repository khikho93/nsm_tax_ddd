﻿namespace LHT_PR.Shared.Base.Interfaces.Storage
{
    public interface IFileResult
    {
        bool Success { get; }
        string Message { get; }
    }

    public interface IDownloadFileResult : IFileResult
    {
        byte[] Data { get; }
    }
}