﻿using LHT_PR.Shared.Base.Interfaces.Data;

namespace LHT_PR.Shared.Base.Shared
{
    public class BaseSort : ISort
    {
        public string Propety { get; set; }
        public bool IsDesc { get; set; }
    }
}