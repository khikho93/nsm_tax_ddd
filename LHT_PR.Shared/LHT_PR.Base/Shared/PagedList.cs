﻿using LHT_PR.Shared.Base.Constants;
using LHT_PR.Shared.Base.Interfaces.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Shared
{
    public class PagedList<T> : IPagedList
    {
        protected PagedList(int pageNo, int pageSize)
        {
            this.PageNo = pageNo;
            this.PageSize = pageSize;
        }

        public int PageSize { get; set; }
        public long TotalRecords { get; set; }

        public int TotalPage
        {
            get
            {
                return (int)(TotalRecords / PageSize) + (TotalRecords % PageSize > 0 ? 1 : 0);
            }
        }

        public int PageNo { get; set; }

        private int Skip
        {
            get
            {
                return (PageSize * (PageNo - 1));
            }
        }

        public IEnumerable<T> DataRows { get; set; }

        public PagedList(IEnumerable<T> data
            , long totalRows
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE
            ) : this(pageNo, pageSize)
        {
            this.TotalRecords = totalRows;
            this.DataRows = data;
            FillOrder();
        }

        private void FillOrder()
        {
            if (typeof(IOrderable).IsAssignableFrom(typeof(T)))
            {
                var startIndex = (PageSize * (PageNo - 1));
                foreach (IOrderable row in DataRows)
                {
                    row.Order = ++startIndex;
                }
            }
        }

        public static async Task<PagedList<T>> InstanceAsync(IQueryable<T> rows
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            int skip = (pageSize * (pageNo - 1));
            long totalRows = rows.LongCount();
            List<T> data = await rows.Skip(skip).Take(pageSize).ToListAsync();

            return new PagedList<T>(data, totalRows, pageNo, pageSize);
        }

        public static PagedList<T> Instance(IQueryable<T> rows
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            return InstanceAsync(rows, pageNo, pageSize).Result;
        }

        public static PagedList<T> Instance(IEnumerable<T> rows
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            int skip = (pageSize * (pageNo - 1));
            long totalRows = rows.LongCount();
            List<T> data = rows.Skip(skip).Take(pageSize).ToList();

            return new PagedList<T>(data, totalRows, pageNo, pageSize);
        }
    }
}