﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace LHT_PR.Shared.Base.DTOs.Storage
{
    public class FileRequestDTO
    {
        public FileRequestDTO()
        {
        }

        public FileRequestDTO(IFormFile file)
        {
            using (var fileStream = file.OpenReadStream())
            {
                var fileData = new byte[fileStream.Length];
                fileStream.Read(fileData, 0, fileData.Length);

                Data = fileData;
                Name = file.FileName;
                Type = file.ContentType;
            }
        }

        private string _realName;
        public string Name { get; set; }
        public string Type { get; set; }

        public byte[] Data { get; set; }

        public string RealName
        {
            get
            {
                return _realName ?? (_realName = $"{Guid.NewGuid().ToString("N")}{Extension}");
            }
        }

        public string Extension
        {
            get
            {
                return Path.GetExtension(Name);
            }
        }
    }
}