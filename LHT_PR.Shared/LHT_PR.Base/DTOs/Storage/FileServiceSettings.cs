﻿namespace LHT_PR.Shared.Base.DTOs.Storage
{
    public class FileServiceSettings
    {
        public string ServerName { get; set; }

        public string User { get; set; }

        public string Password { get; set; }
        public string Port { get; set; }
        public string PrivateFolder { get; set; }
        public string PublicFolder { get; set; }
    }
}