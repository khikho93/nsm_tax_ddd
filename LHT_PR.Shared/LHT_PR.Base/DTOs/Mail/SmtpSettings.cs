﻿namespace LHT_PR.Shared.Base.DTOs.Mail
{
    public class SmtpSettings
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
