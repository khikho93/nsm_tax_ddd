﻿namespace LHT_PR.Shared.Base.DTOs.Mail
{
    public class MailComplieModel
    {
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string FullName { get; set; }
    }
}