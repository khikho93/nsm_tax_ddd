﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class ChangePasswordDTO : LoginDTO
    {
        [Column(IdentityColumns.OLD_PASSWORD)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public string OldPassword { get; set; }

        [Column(IdentityColumns.PASSWORD_CONFIRM)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        [Compare(nameof(Password), ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.ConfirmPasswordInvalid))]
        public string PasswordConfirm { get; set; }
    }
}