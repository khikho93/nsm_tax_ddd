﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class LoginDTO
    {
        [Column(IdentityColumns.USER_OR_EMAIL)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public virtual string UserOrEmail { get; set; }

        [Column(IdentityColumns.PASSWORD)]
        public virtual string Password { get; set; }
    }
}