﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class RegisterDTO : LoginDTO
    {
        [Column(IdentityColumns.PASSWORD_CONFIRM)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        [Compare(nameof(Password), ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.ConfirmPasswordInvalid))]
        public virtual string PasswordConfirm { get; set; }

        [Column(IdentityColumns.PHONE)]
        [Phone(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.PhoneInValid))]
        public virtual string PhoneNumber { get; set; }
    }
}