﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class EmailResetPasswordDTO
    {
        [Column(IdentityColumns.EMAIL)]
        [EmailAddress(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.FormatInvalid))]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public string Email { get; set; }
    }
}