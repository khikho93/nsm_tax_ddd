﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class ResetPasswordDTO : RegisterDTO
    {
        [JsonIgnore]
        public override string PhoneNumber { get; set; }

        [Column(IdentityColumns.TOKEN)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public string Token { get; set; }
    }
}