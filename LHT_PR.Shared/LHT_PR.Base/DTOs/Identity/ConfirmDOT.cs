﻿using LHT_PR.Shared.Base.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static LHT_PR.Shared.Base.Constants.AnnotationConstants;

namespace LHT_PR.Shared.Base.DTOs.Identity
{
    public class ConfirmDTO
    {
        [Column(IdentityColumns.USER_ID)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public long? UserId { get; set; }

        [Column(IdentityColumns.TOKEN)]
        [Required(ErrorMessageResourceType = typeof(AnnotationResource), ErrorMessageResourceName = nameof(AnnotationResource.Required))]
        public string Token { get; set; }
    }
}