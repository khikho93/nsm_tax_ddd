﻿namespace LHT_PR.Shared.Base.Constants
{
    public struct BaseConstants
    {
        public struct MIME_TYPES
        {
            public const string XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }

        public struct ConfigKeys
        {
            public const string JWT_TOKEN = "JwtTokenSettings";
            public const string CONNECTION_STRING = "AppConnectionString";
            public const string SMTP_SETTINGS = "SmtpSettings";
            public const string STORAGE_SETTINGS = "StorageSettings";
        }

        public struct PagingInfo
        {
            public const int DEFAULT_PAGE_SIZE = 20;
            public const int DEFAULT_PAGE_NO = 1;
        }

        public struct AppClaimTypes
        {
            public const string PERMISSION = "http://schemas.microsoft.com/ws/2008/06/identity/claims/permission";
            public const string USER_TYPE = "http://schemas.microsoft.com/ws/2008/06/identity/claims/user-type";
        }

        public struct VerticalAlignments
        {
            public const string TOP = "Top";
            public const string CENTER = "Center";
            public const string BOTTOM = "Bottom";
            public const string DISTRIBUTED = "Distributed";
            public const string JUSTIFY = "Justify";
        }

        public struct HorizontalAlignments
        {
            public const string GENERAL = "General";
            public const string LEFT = "Left";
            public const string CENTER = "Center";
            public const string CENTER_CONTINUOUS = "CenterContinuous";
            public const string RIGHT = "Right";
            public const string FILL = "Fill";
            public const string DISTRIBUTED = "Distributed";
            public const string JUSTIFY = "Justify";
        }
    }
}