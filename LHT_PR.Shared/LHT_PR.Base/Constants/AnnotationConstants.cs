﻿namespace LHT_PR.Shared.Base.Constants
{
    public struct AnnotationConstants
    {
        public struct IdentityColumns
        {
            public const string USER_OR_EMAIL = "Tên tài khoản hoặc email";
            public const string PASSWORD = "Mật khẩu";
            public const string OLD_PASSWORD = "Mật khẩu cũ";
            public const string PASSWORD_CONFIRM = "Xác nhận mật khẩu";
            public const string PHONE = "Số điện thoại";
            public const string FULL_NAME = "Họ và tên";
            public const string USER_ID = "Mã người dùng";
            public const string TOKEN = "Token";
            public const string EMAIL = "Email";
        }
    }
}