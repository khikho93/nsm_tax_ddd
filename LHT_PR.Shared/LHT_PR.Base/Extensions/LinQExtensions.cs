﻿using LHT_PR.Shared.Base.Constants;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LHT_PR.Shared.Base.Extensions
{
    public static class LinQExtensions
    {
        /// <summary>
        /// Paging a collection into a page info for IQueryable
        /// </summary>
        /// <typeparam name="T">Type of item in collection</typeparam>
        /// <param name="source"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedList<T> ToPageList<T>(this IQueryable<T> source
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            return PagedList<T>.Instance(source, pageNo, pageSize);
        }

        /// <summary>
        /// Paging a collection into a page info for IEnumerable
        /// </summary>
        /// <typeparam name="T">Type of item in collection</typeparam>
        /// <param name="source"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedList<T> ToPageList<T>(this IEnumerable<T> source
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            return PagedList<T>.Instance(source, pageNo, pageSize);
        }

        /// <summary>
        /// Paging a collection into a page info for IQueryable
        /// </summary>
        /// <typeparam name="T">Type of item in collection</typeparam>
        /// <param name="source"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static Task<PagedList<T>> ToPageListAsync<T>(this IQueryable<T> source
            , int pageNo = BaseConstants.PagingInfo.DEFAULT_PAGE_NO
            , int pageSize = BaseConstants.PagingInfo.DEFAULT_PAGE_SIZE)
        {
            return PagedList<T>.InstanceAsync(source, pageNo, pageSize);
        }

        public static IOrderedQueryable<T> Sort<T>(this IQueryable<T> source, ISort sort)
        {
            if (String.IsNullOrEmpty(sort?.Propety))
            {
                return (IOrderedQueryable<T>)source;
            }

            var param = Expression.Parameter(typeof(T), "p");
            var prop = Expression.Property(param, sort.Propety);
            var exp = Expression.Lambda(prop, param);
            string method = !sort.IsDesc ? "OrderBy" : "OrderByDescending";
            Type[] types = new Type[] { source.ElementType, exp.Body.Type };
            var mce = Expression.Call(typeof(Queryable), method, types, source.Expression, exp);
            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(mce);
        }
    }
}