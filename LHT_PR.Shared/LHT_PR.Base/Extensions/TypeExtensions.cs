﻿using System;
using System.Linq;
using System.Reflection;

namespace LHT_PR.Shared.Base.Extensions
{
    public static class TypeExtensions
    {
        public static T[] GetConstantValues<T>(this Type type)
        {
            return type
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(T))
                .Select(x => (T)x.GetRawConstantValue())
                .ToArray();
        }

        public static FieldInfo[] GetConstantInfo(this Type type)
        {
            return type
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                .ToArray();
        }
    }
}