﻿using System;

namespace LHT_PR.Shared.Base.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetMessage(this Exception exception)
        {
            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }
            return exception.Message;
        }
    }
}