﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LHT_PR.Shared.Base.Extensions
{
    public static class ObjectExtensions
    {
        public static TClone Clone<TClone>(this object src) where TClone : new()
        {
            var cloneObject = new TClone();
            return Copy<TClone>(src, cloneObject);
        }

        public static TClone Clone<TClone>(this object src, TClone cloneObject)
        {
            return Copy<TClone>(src, cloneObject);
        }

        #region Helpers

        private static IEnumerable<PropertyInfo> CanWriteProperties(Type type)
        {
            return type.GetProperties()
                .Where(p => p.CanWrite);
        }

        private static TCopy Copy<TCopy>(object from, TCopy to)
        {
            var fromProps = CanWriteProperties(from.GetType());
            var toProps = CanWriteProperties(typeof(TCopy));

            foreach (var prop in fromProps)
            {
                var cloneProp = toProps.FirstOrDefault(p => p.Name == prop.Name);
                if (cloneProp != null)
                {
                    var propVal = prop.GetValue(from);
                    cloneProp.SetValue(to, propVal);
                }
                else
                    continue;
            }

            return to;
        }

        #endregion Helpers
    }
}