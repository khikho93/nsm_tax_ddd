﻿using LHT_PR.Shared.Base.Interfaces.Entities;
using System;

namespace LHT_PR.Shared.Base.Entities
{
    public abstract class Audit : EntityBase, IAuditEntity
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
