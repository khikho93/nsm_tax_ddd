﻿using LHT_PR.Shared.Base.Interfaces.Entities;
using System;

namespace LHT_PR.Shared.Base.Entities
{
    public abstract class CoreEntity : EntityBase, IAuditEntity, IDeleteEntity
    {
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
