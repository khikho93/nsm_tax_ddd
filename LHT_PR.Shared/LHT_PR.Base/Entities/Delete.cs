﻿using LHT_PR.Shared.Base.Interfaces.Entities;

namespace LHT_PR.Shared.Base.Entities
{
    public abstract class Delete : EntityBase, IDeleteEntity
    {
        public bool IsDeleted { get; set; }
    }
}
