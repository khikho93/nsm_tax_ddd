﻿using System;
using System.Drawing;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Shared.Base.Attributes.Office
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class ExcelColAttribute : Attribute
    {
        public ExcelColAttribute()
        {
        }

        public int ColSpan { get; set; } = 1;
        public int RowSpan { get; set; } = 1;

        public int ColumnIndex { get; set; }

        public string BackColor { get; set; } = nameof(Color.Transparent);
        public string ForceColor { get; set; } = nameof(Color.Black);

        public string VAlign { get; set; } = VerticalAlignments.CENTER;
        public string HAlign { get; set; } = HorizontalAlignments.LEFT;

        public bool IsBold { get; set; } = false;

        public bool IsItalic { get; set; } = false;
        public int FontSize { get; set; }
        public bool IsBorder { get; set; } = true;
    }
}