﻿using System;
using System.Runtime.CompilerServices;

namespace LHT_PR.Shared.Base.Attributes.Office
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class ExcelSheetAttribute : Attribute
    {
        public ExcelSheetAttribute([CallerMemberName] string sheetName = null)
        {
            SheetName = sheetName;
        }

        public string SheetName { get; set; }
    }
}