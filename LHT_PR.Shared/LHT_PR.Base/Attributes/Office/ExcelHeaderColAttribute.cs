﻿using System;
using System.Runtime.CompilerServices;

namespace LHT_PR.Shared.Base.Attributes.Office
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class ExcelHeaderColAttribute : ExcelColAttribute
    {
        public ExcelHeaderColAttribute([CallerMemberName] string columnName = null)
        {
        }

        public int Width { get; set; }
    }
}