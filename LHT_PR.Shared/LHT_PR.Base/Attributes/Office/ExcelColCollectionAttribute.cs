﻿using System;

namespace LHT_PR.Shared.Base.Attributes.Office
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class ExcelColCollectionAttribute : Attribute
    {
        public ExcelColCollectionAttribute()
        {
        }
    }
}