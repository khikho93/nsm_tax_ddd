﻿using LHT_PR.Shared.Base.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models
{
    [Table("Languages")]
    public class Language : CoreEntity
    {
        [Required]
        [StringLength(50)]
        public string Key { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public Guid? IconId { get; set; }

        [ForeignKey(nameof(IconId))]
        public virtual Attachment Icon { get; set; }
    }
}