﻿using LHT_PR.Shared.Base.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models.Identity
{
    [Table("RefreshTokens")]
    public class RefreshToken : EntityBase
    {
        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [Required]
        [StringLength(256)]
        public string Token { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}