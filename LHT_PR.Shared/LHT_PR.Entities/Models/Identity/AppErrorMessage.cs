﻿using LHT_PR.Shared.Base.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models.Identity
{
    [Table("AppErrorMessage")]
    public class AppErrorMessage : Audit
    {
        [Required]
        [StringLength(500)]
        public string ErrorMessage { get; set; }

        [Required]
        public string Trace { get; set; }
    }
}