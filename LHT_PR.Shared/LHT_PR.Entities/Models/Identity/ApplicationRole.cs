﻿using LHT_PR.Shared.Base.Interfaces.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace LHT_PR.Shared.Entities.Models.Identity
{
    public class ApplicationRole : IdentityRole<Guid>, IAuditEntity
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}