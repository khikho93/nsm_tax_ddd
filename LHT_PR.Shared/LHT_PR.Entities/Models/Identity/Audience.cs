﻿using LHT_PR.Shared.Base.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models.Identity
{
    [Table("Audiences")]
    public class Audience : CoreEntity
    {
        public string Issuer { get; set; }
        public string AudienceName { get; set; }
        public string SecretKey { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}