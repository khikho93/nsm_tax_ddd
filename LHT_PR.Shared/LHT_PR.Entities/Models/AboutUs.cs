﻿using LHT_PR.Shared.Base.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models
{
    [Table("AboutUs")]
    public class AboutUs : EntityBase
    {
        [StringLength(100)]
        [Required]
        public string Key { get; set; }

        [StringLength(1000)]
        [Required]
        public string Value { get; set; }

        public Guid LangId { get; set; }

        [ForeignKey(nameof(LangId))]
        public virtual Language Language { get; set; }
    }
}