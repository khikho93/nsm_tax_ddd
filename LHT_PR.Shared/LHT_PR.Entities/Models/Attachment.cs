﻿using LHT_PR.Shared.Base.Entities;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LHT_PR.Shared.Entities.Models
{
    [Table("Attachments")]
    public class Attachment : CoreEntity
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public string RealName { get; set; }
        public string Decription { get; set; }
        public bool IsSecure { get; set; }

        [Required]
        [StringLength(50)]
        public string Extension { get; set; }

        [StringLength(128)]
        public string Type { get; set; }

        public virtual Collection<Language> Languages { get; set; }
    }
}