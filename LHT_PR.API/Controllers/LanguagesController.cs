﻿using LHT_PR.Domain.ViewModels.Request.Languages;
using LHT_PR.Worker.Domain.Constants;
using LHT_PR.Worker.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LHT_PR.Worker.API.Controllers
{
    [Route(LanguageRoute.Prefix)]
    [ApiController]
    public class LanguagesController : BaseApiController
    {
        private readonly ILanguagesService _services;

        public LanguagesController(ILanguagesService services
            )
        {
            _services = services;
        }

        [HttpGet]
        [Route(LanguageRoute.List)]
        public async Task<ActionResult> SelectItem([FromQuery]AllLanguageRequest request)
        {
            var response = await _services.SelectItemsAsync(request);
            return StatusCode(response);
        }
    }
}