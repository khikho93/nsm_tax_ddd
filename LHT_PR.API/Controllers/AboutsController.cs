﻿using LHT_PR.Domain.ViewModels.Request.Abouts;
using LHT_PR.Worker.Domain.Constants;
using LHT_PR.Worker.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LHT_PR.Worker.API.Controllers
{
    [Route(AboutRoute.PREFIX)]
    [ApiController]
    public class AboutsController : BaseApiController
    {
        private readonly IAboutsService _services;

        public AboutsController(IAboutsService services
            )
        {
            _services = services;
        }

        [HttpPost]
        [Route(AboutRoute.CREATE)]
        public async Task<ActionResult> Create([FromBody]CreateAboutRequest request)
        {
            var response = await _services.CreateAsync(request);
            return StatusCode(response);
        }

        [HttpGet]
        [Route(AboutRoute.HOME_INFO)]
        public async Task<ActionResult> HomeInfo([FromRoute]AboutInfoRequest request)
        {
            var response = await _services.HomeInfoAsync(request);
            return StatusCode(response);
        }

        [HttpGet]
        [Route(AboutRoute.ABOUT_TITLE_INFO)]
        public async Task<ActionResult> AboutTitleInfo([FromRoute]AboutInfoRequest request)
        {
            var response = await _services.AboutTitleInfoAsync(request);
            return StatusCode(response);
        }

        [HttpGet]
        [Route(AboutRoute.ABOUT_DESCRIPTION_INFO)]
        public async Task<ActionResult> AboutDescriptionInfo([FromRoute]AboutInfoRequest request)
        {
            var response = await _services.AboutDescriptionInfoAsync(request);
            return StatusCode(response);
        }

        [HttpGet]
        [Route(AboutRoute.STRATEGIC_PARTNER_INFO)]
        public async Task<ActionResult> StrategicPartnerInfo([FromRoute]AboutInfoRequest request)
        {
            var response = await _services.StrategicPartnerInfoAsync(request);
            return StatusCode(response);
        }

        [HttpGet]
        [Route(AboutRoute.SERVICE_INFO)]
        public async Task<ActionResult> ServiceInfo([FromRoute]AboutInfoRequest request)
        {
            var response = await _services.ServiceInfoAsync(request);
            return StatusCode(response);
        }
    }
}