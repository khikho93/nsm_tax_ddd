﻿using LHT_PR.Shared.Base.Interfaces.Http;
using LHT_PR.Shared.Entities.Models.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Worker.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class BaseApiController : ControllerBase
    {
        protected UserManager<ApplicationUser> UserManager
        {
            get
            {
                return (UserManager<ApplicationUser>)HttpContext.RequestServices.GetService(typeof(UserManager<ApplicationUser>));
            }
        }

        [NonAction]
        public virtual ActionResult StatusCode(IHttpResponse response)
        {
            return StatusCode(response.StatusCode, response);
        }
    }
}