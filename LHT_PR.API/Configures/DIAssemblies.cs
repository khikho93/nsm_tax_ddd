﻿using LHT_PR.Infrastructure.DBContext;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Utility.Data;
using LHT_PR.Worker.Domain.Services;
using System.Reflection;

namespace LHT_PR.Worker.API.Configures
{
    public static class DIAssemblies
    {
        internal static Assembly[] AssembliesToScan = new Assembly[]
        {
            Assembly.GetExecutingAssembly(),
            Assembly.GetAssembly(typeof(IRepositoryBase<>)),
            Assembly.GetAssembly(typeof(RepositoryBase<>)),
            Assembly.GetAssembly(typeof(CommonService)),
            Assembly.GetAssembly(typeof(AppDbContext))
        };
    }
}