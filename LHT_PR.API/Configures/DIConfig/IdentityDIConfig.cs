﻿using LHT_PR.Infrastructure.DBContext;
using LHT_PR.Shared.Base.Identity;
using LHT_PR.Shared.Base.Interfaces.Identity;
using LHT_PR.Shared.Entities.Models.Identity;
using LHT_PR.Shared.Utility.Identity;
using LHT_PR.Shared.Utility.Identity.Policies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Worker.API.Configures.DIConfig
{
    public static class IdentityDIConfig
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            // Inject Identity
            services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddEntityFrameworkStores<AppDbContext>()
            .AddDefaultTokenProviders();

            // Config JWT
            services.Configure<JwtTokenSettings>(configuration.GetSection(ConfigKeys.JWT_TOKEN));
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.AddScoped<IJwtProvider, JwtProvider>();
            services.AddScoped<IJwtGenerator, JwtGenerator>();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // remove default claims

            // configure Jwt
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                //cfg.ClaimsIssuer            = tokenSettings.Issuer;
                cfg.TokenValidationParameters = GetTokenValidationParameters(configuration);
                cfg.Events = GetTokenValidationEvents();
            });

            AddAuthorizationPolicies(services);
        }

        private static TokenValidationParameters GetTokenValidationParameters(IConfiguration configuration)
        {
            var tokenSettings = configuration.GetSection(ConfigKeys.JWT_TOKEN).Get<JwtTokenSettings>();

            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = tokenSettings.Issuer,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(tokenSettings.SecretKey)),

                ValidateAudience = true,
                ValidAudience = tokenSettings.Audience,

                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero, // remove delay of token when expire
            };
        }

        private static JwtBearerEvents GetTokenValidationEvents()
        {
            return new JwtBearerEvents
            {
                OnAuthenticationFailed = context =>
                {
                    if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                    {
                        context.Response.Headers.Add("Token-Expired", "true");
                    }
                    return Task.CompletedTask;
                }
            };
        }

        private static void AddAuthorizationPolicies(IServiceCollection services)
        {
            // configure policies
            services.AddAuthorization(options =>
            {
            });

            services.AddSingleton<IAuthorizationHandler, UserTypePolicyHandler>();
        }
    }
}