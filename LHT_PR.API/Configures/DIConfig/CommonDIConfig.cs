﻿using AutoMapper;
using LHT_PR.Shared.Base.DTOs.Mail;
using LHT_PR.Shared.Base.DTOs.Storage;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Interfaces.Helper;
using LHT_PR.Shared.Base.Interfaces.Logging;
using LHT_PR.Shared.Utility.Data;
using LHT_PR.Shared.Utility.Helpers;
using LHT_PR.Shared.Utility.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetCore.AutoRegisterDi;
using System.Linq;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Worker.API.Configures.DIConfig
{
    public static class CommonDIConfig
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            // Inject Services
            services
            .RegisterAssemblyPublicNonGenericClasses(DIAssemblies.AssembliesToScan)
                .Where(t => t.Name.EndsWith("Services"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            services
            .RegisterAssemblyPublicNonGenericClasses(DIAssemblies.AssembliesToScan)
                .Where(t => t.Name.EndsWith("Service"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            // Inject Repositories
            services
            .RegisterAssemblyPublicNonGenericClasses(DIAssemblies.AssembliesToScan)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            // Inject Helpers
            services
            .RegisterAssemblyPublicNonGenericClasses(DIAssemblies.AssembliesToScan)
                .Where(t => t.Name.EndsWith("Helper"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            // Inject Handlers
            services
            .RegisterAssemblyPublicNonGenericClasses(DIAssemblies.AssembliesToScan)
                .Where(t => t.Name.EndsWith("Handler"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));

            // Inject AutoMapper
            services.AddAutoMapper(DIAssemblies.AssembliesToScan);

            // Inject Mail Helpers
            services.Configure<SmtpSettings>(configuration.GetSection(ConfigKeys.SMTP_SETTINGS));
            services.AddScoped<IMailHelper, MailHelper>();
            services.AddScoped<IMailBodyGenerator, MailBodyGenerator>();

            services.AddScoped<IAppLogger, AppLogger>();

            // Storage
            services.Configure<FileServiceSettings>(configuration.GetSection(ConfigKeys.STORAGE_SETTINGS));
        }
    }
}