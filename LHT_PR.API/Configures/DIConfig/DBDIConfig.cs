﻿using LHT_PR.Infrastructure.DBContext;
using LHT_PR.Shared.Base.Interfaces.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static LHT_PR.Shared.Base.Constants.BaseConstants;

namespace LHT_PR.Worker.API.Configures.DIConfig
{
    public static class DBDIConfig
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            // Inject DbContext
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseNpgsql(configuration.GetConnectionString(ConfigKeys.CONNECTION_STRING));
            });

            // Inject UnitOfWork
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDbFactory, DbFactory>();
        }
    }
}