﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace LHT_PR.Worker.API.Configures
{
    public static class SwaggerConfig
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Title = "LHT PR Comments Collection APIs UIs",
                    Version = "v1"
                });
            });
        }

        public static void Enable(IApplicationBuilder builder)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint
            builder.UseSwagger();

            // Enable middleware to serve swagger-ui(HTML, JS, CSS, etc,),
            // specifying the Swagger  JSON endpoint
            builder.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./swagger/v1/swagger.json", "LHT PR Comments Collection APIs UIs v1");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}