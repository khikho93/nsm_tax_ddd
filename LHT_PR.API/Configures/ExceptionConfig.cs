﻿using LHT_PR.Worker.Domain.Services;
using LHT_PR.Shared.Base.Extensions;
using LHT_PR.Shared.Base.Interfaces.Logging;
using LHT_PR.Shared.Base.Shared.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using System.Text.Json;

namespace LHT_PR.Worker.API.Configures
{
    public static class ExceptionConfig
    {
        public static void Configure(IApplicationBuilder app)
        {
            app.UseExceptionHandler(err => err.Run(async context =>
            {
                var exHandlerPathFeature =
                    context.Features.Get<IExceptionHandlerPathFeature>();

                // Use exceptionHandlerPathFeature to process the exception (for example,
                // logging), but do NOT expose sensitive error information directly to
                // the client.

                if (exHandlerPathFeature?.Error != null)
                {
                    var exception = exHandlerPathFeature?.Error;
                    if (exception != null)
                    {
                        //var exHandler = context.RequestServices.GetService<IErrorMessagesServices>();
                        var appLogger = context.RequestServices.GetService<IAppLogger>();

                        //await exHandler?.CreateAsync(exception);
                        appLogger?.Error(exception);

                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";

                        var response = HttpResponse<string>.Error(exception.GetMessage(), (HttpStatusCode)context.Response.StatusCode);
                        await context.Response.WriteAsync(JsonSerializer.Serialize(response));
                    }
                }
            }));
        }
    }
}