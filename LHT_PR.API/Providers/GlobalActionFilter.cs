﻿using LHT_PR.Worker.Domain.ViewModels.Request;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace LHT_PR.API.Providers
{
    public class GlobalActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Headers.ContainsKey("lang"))
            {
                if (context.ActionArguments.Count > 0)
                {
                    foreach (var arg in context.ActionArguments)
                    {
                        if (typeof(BaseRequest).IsAssignableFrom(arg.Value.GetType()))
                        {
                            (arg.Value as BaseRequest).LangKey = context.HttpContext.Request.Headers["lang"];
                        }
                    }
                }
            }
        }
    }

    public class AsyncGlobalActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.HttpContext.Request.Headers.ContainsKey("lang"))
            {
                if (context.ActionArguments.Count > 0)
                {
                    foreach (var arg in context.ActionArguments)
                    {
                        if (typeof(BaseRequest).IsAssignableFrom(arg.Value.GetType()))
                        {
                            (arg.Value as BaseRequest).LangKey = context.HttpContext.Request.Headers["lang"];
                        }
                    }
                }
            }

            var result = await next();
        }
    }
}