using LHT_PR.API.Providers;
using LHT_PR.Infrastructure.DBContext;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Worker.API.Configures;
using LHT_PR.Worker.API.Configures.DIConfig;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;

namespace LHT_PR.Worker.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMemoryCache();
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options =>
                {
                    options.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Token-Expired");
                });
            });
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
                //options.Filters.Add(new GlobalActionFilter());
                options.Filters.Add(new AsyncGlobalActionFilter());
            })
            .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Latest);

            DBDIConfig.Configure(services, Configuration);
            IdentityDIConfig.Configure(services, Configuration);
            CommonDIConfig.Configure(services, Configuration);
            SwaggerConfig.Configure(services, Configuration);
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app
            , IWebHostEnvironment env
            , AppDbContext iTRContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            ExceptionConfig.Configure(app);

            app.UseHttpsRedirection();

            try
            {
                iTRContext.Database.Migrate();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            SwaggerConfig.Enable(app);
            HttpAppContext.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseRouting();

            app.UseAuthentication();
            app.UseCors("AllowOrigin");
            app.UseMvc();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }
    }
}