﻿using LHT_PR.Domain.ViewModels.Response;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using LHT_PR.Worker.Domain.ViewModels.Request;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.Json.Serialization;

namespace LHT_PR.Domain.ViewModels.Request.Abouts
{
    public class StrategicPartnerInfoRequest : BaseRequest
        , IRequest<HttpResponse<List<BaseKeyValueResponse>>>
        , IQuerySelection<AboutUs, BaseKeyValueResponse>
        , IFilterExpression<AboutUs>
    {
        [JsonIgnore]
        public List<string> Keys { get; set; }

        public Expression<Func<AboutUs, bool>> GetExpression()
        {
            return a => Keys.Contains(a.Key)
                        && a.Language.Key == LangKey;
        }

        public Expression<Func<AboutUs, BaseKeyValueResponse>> GetSelection()
        {
            return a => new BaseKeyValueResponse()
            {
                Key = a.Key,
                Value = a.Value
            };
        }
    }
}