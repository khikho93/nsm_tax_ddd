﻿using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using LHT_PR.Worker.Domain.ViewModels.Request;
using MediatR;
using System;

namespace LHT_PR.Domain.ViewModels.Request.Abouts
{
    public class CreateAboutRequest : BaseRequest
        , IRequest<HttpResponse<Guid>>
        , IEntityConvert<AboutUs>
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public AboutUs GetEntity<TSource>(TSource source)
        {
            throw new NotImplementedException();
        }

        public AboutUs GetEntity()
        {
            return new AboutUs()
            {
                Key = Key,
                Value = Value
            };
        }

        public AboutUs GetEntity(AboutUs source)
        {
            throw new NotImplementedException();
        }
    }
}