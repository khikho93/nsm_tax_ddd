﻿using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared;
using LHT_PR.Worker.Domain.Constants;
using System;
using System.Linq.Expressions;

namespace LHT_PR.Worker.Domain.ViewModels.Request
{
    public abstract class BaseListFilter<T>
        : BaseRequest, IFilterExpression<T>, IPagingInfo
    {
        public int PageNo { get; set; } = AppConstants.PagingInfo.DEFAULT_PAGE_NO;
        public int PageSize { get; set; } = AppConstants.PagingInfo.DEFAULT_PAGE_SIZE;

        public BaseSort Sort { get; set; }

        public abstract Expression<Func<T, bool>> GetExpression();
    }
}