﻿using LHT_PR.Shared.Base.Interfaces.Entities;
using System;

namespace LHT_PR.Worker.Domain.ViewModels.Request
{
    public class BaseEntityRequest : IEntity
    {
        public Guid Id { get; set; }
    }
}