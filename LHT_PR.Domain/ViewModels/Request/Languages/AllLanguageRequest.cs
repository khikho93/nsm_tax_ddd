﻿using LHT_PR.Domain.ViewModels.Response.Languages;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace LHT_PR.Domain.ViewModels.Request.Languages
{
    public class AllLanguageRequest :
        IRequest<HttpResponse<List<LanguageItemResponse>>>
        , IQuerySelection<Language, LanguageItemResponse>
    {
        public Expression<Func<Language, LanguageItemResponse>> GetSelection()
        {
            return l => new LanguageItemResponse()
            {
                Id = l.Id,
                Key = l.Key,
                Name = l.Name,
                Icon = l.Icon.RealName
            };
        }
    }
}