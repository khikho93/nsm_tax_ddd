﻿using System.Text.Json.Serialization;
using static LHT_PR.Worker.Domain.Constants.AppConstants;

namespace LHT_PR.Worker.Domain.ViewModels.Request
{
    public class BaseRequest
    {
        [JsonIgnore]
        public string LangKey { get; set; } = LanguageKeys.ENGLISH;
    }
}