﻿using LHT_PR.Domain.ViewModels.Response.Common;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using MediatR;
using System;
using System.Linq.Expressions;

namespace LHT_PR.Domain.ViewModels.Request.Common
{
    public class GetLangInfoRequest : IRequest<HttpResponse<LanguageInfoResponse>>
        , IFilterExpression<Language>
        , IQuerySelection<Language, LanguageInfoResponse>
    {
        public string LanguageKey { get; set; }

        public Expression<Func<Language, bool>> GetExpression()
        {
            return l => l.Key == LanguageKey;
        }

        public Expression<Func<Language, LanguageInfoResponse>> GetSelection()
        {
            return l => new LanguageInfoResponse()
            {
                Id = l.Id,
                Key = l.Key,
                Name = l.Name
            };
        }
    }
}