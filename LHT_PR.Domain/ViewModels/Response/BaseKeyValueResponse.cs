﻿namespace LHT_PR.Domain.ViewModels.Response
{
    public class BaseKeyValueResponse
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}