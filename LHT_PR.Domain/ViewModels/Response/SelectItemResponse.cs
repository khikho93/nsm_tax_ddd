﻿using LHT_PR.Worker.Domain.ViewModels.Request;

namespace LHT_PR.Worker.Domain.ViewModels.Response
{
    public class SelectItemResponse : BaseEntityRequest
    {
        public string Name { get; set; }
    }
}