﻿using LHT_PR.Worker.Domain.ViewModels.Response;

namespace LHT_PR.Domain.ViewModels.Response.Common
{
    public class LanguageInfoResponse : SelectItemResponse
    {
        public string Key { get; set; }
    }
}