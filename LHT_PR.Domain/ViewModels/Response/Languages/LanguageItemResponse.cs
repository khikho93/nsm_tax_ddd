﻿using LHT_PR.Domain.ViewModels.Response.Common;

namespace LHT_PR.Domain.ViewModels.Response.Languages
{
    public class LanguageItemResponse : LanguageInfoResponse
    {
        public string Icon { get; set; }
    }
}