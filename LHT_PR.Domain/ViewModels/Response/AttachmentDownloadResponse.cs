﻿namespace LHT_PR.Worker.Domain.ViewModels.Response
{
    public class AttachmentDownloadResponse
    {
        public string Name { get; set; }
        public byte[] FileContent { get; set; }
        public string Type { get; set; }
    }
}