﻿using AutoMapper;

namespace LHT_PR.Worker.Domain.Config.MapperConfig
{
    public class UserMapperProfiles : Profile
    {
        public UserMapperProfiles()
        {
        }

        private string Split(string text, char c, int index)
        {
            return text.Split(c)[index];
        }
    }
}