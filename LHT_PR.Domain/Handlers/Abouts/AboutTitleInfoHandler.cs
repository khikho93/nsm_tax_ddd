﻿using LHT_PR.Domain.ViewModels.Request.Abouts;
using LHT_PR.Domain.ViewModels.Response;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using LHT_PR.Shared.Utility.Http.HttpHandlers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Domain.Handlers.Abouts
{
    public class AboutTitleInfoHandler : BaseHandler, IRequestHandler<AboutInfoRequest, HttpResponse<List<BaseKeyValueResponse>>>
    {
        private readonly IRepositoryBase<AboutUs> _aboutRepo;

        public AboutTitleInfoHandler(
            IRepositoryBase<AboutUs> aboutRepo
            )
        {
            _aboutRepo = aboutRepo;
        }

        public async Task<HttpResponse<List<BaseKeyValueResponse>>> Handle(AboutInfoRequest request, CancellationToken cancellationToken)
        {
            var items = await _aboutRepo.List(request.GetExpression())
                .Select(request.GetSelection())
                .ToListAsync();

            return HttpResponse<List<BaseKeyValueResponse>>.OK(items);
        }
    }
}