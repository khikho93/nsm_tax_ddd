﻿using LHT_PR.Domain.ViewModels.Request.Abouts;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using LHT_PR.Shared.Utility.Http.HttpHandlers;
using LHT_PR.Worker.Domain.Services;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Domain.Handlers.Abouts
{
    public class CreateAboutHandler : BaseHandler, IRequestHandler<CreateAboutRequest, HttpResponse<Guid>>
    {
        private readonly IRepositoryBase<AboutUs> _aboutRepo;
        private readonly ICommonService _commonService;

        public CreateAboutHandler(
            IRepositoryBase<AboutUs> aboutRepo
            , ICommonService commonService
            )
        {
            _aboutRepo = aboutRepo;
            _commonService = commonService;
        }

        public async Task<HttpResponse<Guid>> Handle(CreateAboutRequest request, CancellationToken cancellationToken)
        {
            var langId = await _commonService.LangInfoAsync(request.LangKey);

            var aboutItem = request.GetEntity();
            aboutItem.LangId = langId.Data.Id;

            _aboutRepo.Insert(aboutItem);

            var saved = await UnitOfWork.CommitAsync();

            if (saved > 0)
            {
                return HttpResponse<Guid>.OK(aboutItem.Id);
            }
            else
                return HttpResponse<Guid>.Error(statusCode: System.Net.HttpStatusCode.NotModified);
        }
    }
}