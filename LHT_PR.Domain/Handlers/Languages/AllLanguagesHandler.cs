﻿using LHT_PR.Domain.ViewModels.Request.Languages;
using LHT_PR.Domain.ViewModels.Response.Languages;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Interfaces.Storage;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Domain.Handlers.Languages
{
    public class AllLanguagesHandler : IRequestHandler<AllLanguageRequest, HttpResponse<List<LanguageItemResponse>>>
    {
        private readonly IHttpFileService _fileService;
        private readonly IRepositoryBase<Language> _langRepo;

        public AllLanguagesHandler(IRepositoryBase<Language> langRepo
            , IHttpFileService fileService)
        {
            _fileService = fileService;
            _langRepo = langRepo;
        }

        public async Task<HttpResponse<List<LanguageItemResponse>>> Handle(AllLanguageRequest request, CancellationToken cancellationToken)
        {
            var allLangs = await _langRepo.All()
                .Select(request.GetSelection())
                .OrderBy(l => l.Key)
                .ToListAsync();

            allLangs.ForEach(l => l.Icon = _fileService.GetFileUri(l.Icon, false));

            return HttpResponse<List<LanguageItemResponse>>.OK(allLangs);
        }
    }
}