﻿using LHT_PR.Domain.ViewModels.Request.Common;
using LHT_PR.Domain.ViewModels.Response.Common;
using LHT_PR.Shared.Base.Interfaces.Data;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Entities.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LHT_PR.Domain.Handlers.Common
{
    public class GetLangInfoHandler : IRequestHandler<GetLangInfoRequest, HttpResponse<LanguageInfoResponse>>
    {
        private readonly IRepositoryBase<Language> _langRepo;

        public GetLangInfoHandler(IRepositoryBase<Language> langRepo)
        {
            _langRepo = langRepo;
        }

        public async Task<HttpResponse<LanguageInfoResponse>> Handle(GetLangInfoRequest request, CancellationToken cancellationToken)
        {
            var langInfo = await _langRepo.List(request.GetExpression())
                .Select(request.GetSelection())
                .FirstOrDefaultAsync();

            if (langInfo != null)
            {
                return HttpResponse<LanguageInfoResponse>.OK(langInfo);
            }
            else
                return HttpResponse<LanguageInfoResponse>.Error(statusCode: System.Net.HttpStatusCode.NoContent);
        }
    }
}