﻿using LHT_PR.Domain.ViewModels.Request.Abouts;
using LHT_PR.Domain.ViewModels.Response;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LHT_PR.Worker.Domain.Services
{
    public interface IAboutsService
    {
        Task<HttpResponse<Guid>> CreateAsync(CreateAboutRequest request);

        Task<HttpResponse<List<BaseKeyValueResponse>>> HomeInfoAsync(AboutInfoRequest request);

        Task<HttpResponse<List<BaseKeyValueResponse>>> AboutTitleInfoAsync(AboutInfoRequest request);

        Task<HttpResponse<List<BaseKeyValueResponse>>> AboutDescriptionInfoAsync(AboutInfoRequest request);

        Task<HttpResponse<List<BaseKeyValueResponse>>> StrategicPartnerInfoAsync(AboutInfoRequest request);

        Task<HttpResponse<List<BaseKeyValueResponse>>> ServiceInfoAsync(AboutInfoRequest request);
    }

    public class AboutsService : BaseDomainService, IAboutsService
    {
        public Task<HttpResponse<List<BaseKeyValueResponse>>> AboutDescriptionInfoAsync(AboutInfoRequest request)
        {
            request.Keys = new List<string>() { "AboutTitle", "AboutDes1", "AboutDes2" };
            return Mediator.Send(request);
        }

        public Task<HttpResponse<List<BaseKeyValueResponse>>> AboutTitleInfoAsync(AboutInfoRequest request)
        {
            request.Keys = new List<string>() { "AboutTitle", "TeamTarget" };
            return Mediator.Send(request);
        }

        public Task<HttpResponse<List<BaseKeyValueResponse>>> ServiceInfoAsync(AboutInfoRequest request)
        {
            request.Keys = new List<string>() { "ServiceTitle", "ServiceDescription" };
            return Mediator.Send(request);
        }

        public Task<HttpResponse<Guid>> CreateAsync(CreateAboutRequest request)
        {
            return Mediator.Send(request);
        }

        public Task<HttpResponse<List<BaseKeyValueResponse>>> HomeInfoAsync(AboutInfoRequest request)
        {
            request.Keys = new List<string>() { "Slogan", "SloganDes" };
            return Mediator.Send(request);
        }

        public Task<HttpResponse<List<BaseKeyValueResponse>>> StrategicPartnerInfoAsync(AboutInfoRequest request)
        {
            request.Keys = new List<string>() { "StrategicPartnerTitle", "StrategicPartnerContent" };
            return Mediator.Send(request);
        }
    }
}