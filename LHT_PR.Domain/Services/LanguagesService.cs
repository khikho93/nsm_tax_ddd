﻿using LHT_PR.Domain.ViewModels.Request.Languages;
using LHT_PR.Domain.ViewModels.Response.Languages;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LHT_PR.Worker.Domain.Services
{
    public interface ILanguagesService
    {
        Task<HttpResponse<List<LanguageItemResponse>>> SelectItemsAsync(AllLanguageRequest request);
    }

    public class LanguagesService : BaseDomainService, ILanguagesService
    {
        public Task<HttpResponse<List<LanguageItemResponse>>> SelectItemsAsync(AllLanguageRequest request)
        {
            return Mediator.Send(request);
        }
    }
}