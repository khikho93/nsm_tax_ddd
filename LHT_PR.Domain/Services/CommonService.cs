﻿using LHT_PR.Domain.ViewModels.Request.Common;
using LHT_PR.Domain.ViewModels.Response.Common;
using LHT_PR.Shared.Base.Shared.Http;
using LHT_PR.Shared.Utility.Services;
using System.Threading.Tasks;

namespace LHT_PR.Worker.Domain.Services
{
    public interface ICommonService
    {
        Task<HttpResponse<LanguageInfoResponse>> LangInfoAsync(string langKey);
    }

    public class CommonService : BaseDomainService, ICommonService
    {
        public Task<HttpResponse<LanguageInfoResponse>> LangInfoAsync(string langKey)
        {
            var request = new GetLangInfoRequest()
            {
                LanguageKey = langKey
            };

            return Mediator.Send(request);
        }
    }
}