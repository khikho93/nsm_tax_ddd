﻿namespace LHT_PR.Worker.Domain.Constants
{
    public static class UsersRoute
    {
        public const string TOKEN = "token";
        public const string Prefix = @"api/users";
        public const string LIST = "list";
        public const string REGISTER = "register-account";
        public const string CHANGE_PASSWORD = "change-password";
        public const string CONFIRM = "confirm-account";
        public const string UPDATE_PROFILE = "update-general-info";
        public const string GETIDINFO = "get-general-info";
        public const string SEND_RESET_PASSWORD_LINK = "send-reset-password-link";
        public const string RESET_PASSWORD = "reset-password";
        public const string UPLOADFILES = "upload-files";
        public const string VISIBLE_PROFILE = "visible-profile";
        public const string FORGOT_PASSWORD_LINK = "forgot-password";
        public const string CERTIFICATES = "certificates";
        public const string SAVE_CERTIFICATE = "save-certificate";
        public const string SAVE_JOB_EXP = "save-job-exp";
        public const string JOB_EXPS = "job-exp-list";
        public const string SKILLS = "skills-list";
        public const string DELETECERTIFICATE = "delete-certificate";
        public const string SAVE_SKILLS = "save-skills";
        public const string SAVE_FOREIGN_LANGUAGES = "save-foreign-languages";
        public const string FOREIGN_LANGUAGES = "foreign-languages-lists";
        public const string DELETE_JOB_EXP = "delete-job-exp";
        public const string DASHBOARD = "dasboard-sumary-info";
    }

    public struct LanguageRoute
    {
        public const string Prefix = @"api/languages";
        public const string List = @"select-items";
    }

    public struct AboutRoute
    {
        public const string PREFIX = @"api/about";
        public const string CREATE = @"create";
        public const string HOME_INFO = @"home-info";
        public const string ABOUT_TITLE_INFO = @"about-title-info";
        public const string SERVICE_INFO = @"service-info";
        public const string ABOUT_DESCRIPTION_INFO = @"about-description-info";
        public const string STRATEGIC_PARTNER_INFO = @"strategic-partner-info";
    }

    public struct StrategicPartnerRoute
    {
        public const string PREFIX = @"api/strategic-partners";
        public const string CREATE = @"create";
        public const string LIST = @"list";
    }

    public struct EndToEndSolutionRoute
    {
        public const string PREFIX = @"api/end-to-end-solutions";
        public const string CREATE = @"create";
        public const string LIST = @"list";
    }

    public struct ServiceRoute
    {
        public const string PREFIX = @"api/services";
        public const string CREATE = @"create";
        public const string LIST = @"list";
    }

    public struct SkillRoute
    {
        public const string PREFIX = @"api/skills";
        public const string CREATE = @"create";
        public const string LIST = @"list";
    }
}