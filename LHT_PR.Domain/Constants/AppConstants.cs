﻿namespace LHT_PR.Worker.Domain.Constants
{
    public struct AppConstants
    {
        public struct PagingInfo
        {
            public const int DEFAULT_PAGE_SIZE = 20;
            public const int DEFAULT_PAGE_NO = 1;
        }

        public struct LanguageKeys
        {
            public const string ENGLISH = "en";
            public const string VIETNAMESE = "vi";
        }

        public struct AppServiceTypes
        {
            public const string DEVELOPMENT = "Development";
            public const string QUALITY_ENGINEERING = "Quality Engineering";
            public const string DELIVERY_PROCESSES = "Delivery Processes";
        }

        public struct AppSkillTypes
        {
            public const string WEB_TECHNOLOGIES = "Web Technologies";
            public const string MOBILE_TECHNOLOGIES = "Mobile Technologies";
            public const string CLOUD_SERVICES = "Cloud Services";
        }
    }
}