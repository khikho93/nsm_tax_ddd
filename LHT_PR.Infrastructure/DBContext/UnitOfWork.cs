﻿using LHT_PR.Shared.Base.Interfaces.Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace LHT_PR.Infrastructure.DBContext
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;

        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> CommitAsync()
        {
            return _dbContext.SaveChangesAsync();
        }

        public DbContext CreateDbContext()
        {
            return _dbContext;
        }
    }
}