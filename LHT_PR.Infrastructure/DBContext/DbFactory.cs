﻿using LHT_PR.Shared.Base.Interfaces.Database;
using Microsoft.EntityFrameworkCore;

namespace LHT_PR.Infrastructure.DBContext
{
    public class DbFactory : IDbFactory
    {
        public DbFactory(AppDbContext context)
        {
            DbContext = context;
        }

        public DbContext DbContext { get; set; }
    }
}