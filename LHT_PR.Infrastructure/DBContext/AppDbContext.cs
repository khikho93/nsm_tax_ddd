﻿using LHT_PR.Infrastructure.EntitiesConfig;
using LHT_PR.Shared.Entities.Models;
using LHT_PR.Shared.Entities.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace LHT_PR.Infrastructure.DBContext
{
    public class AppDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, IdentityUserClaim<Guid>, ApplicationUserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public DbSet<Audience> Audiences { get; set; }
        public DbSet<AppErrorMessage> AppErrorMessages { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<Language> Languages { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var attachConfig = new AttachmentConfig();
            modelBuilder.ApplyConfiguration(attachConfig);
            modelBuilder.ApplyConfiguration(new LanguageConfig(attachConfig.AddedIds));
            base.OnModelCreating(modelBuilder);
        }
    }
}