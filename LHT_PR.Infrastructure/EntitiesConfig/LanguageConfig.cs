﻿using LHT_PR.Shared.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using static LHT_PR.Worker.Domain.Constants.AppConstants;

namespace LHT_PR.Infrastructure.EntitiesConfig
{
    internal class LanguageConfig : IEntityTypeConfiguration<Language>
    {
        private readonly Dictionary<string, Guid> _attachedIds;

        public LanguageConfig(Dictionary<string, Guid> attachedIds)
        {
            _attachedIds = attachedIds;
        }

        public void Configure(EntityTypeBuilder<Language> builder)
        {
            Guid enId = new Guid("0754c029-e94b-43f8-b252-4edb7a8b1a96"),
                 viId = new Guid("57621482-8c00-40c2-961a-98bb1f910159");

            builder.HasData(
                new Language
                {
                    Id = enId,
                    CreatedBy = "SystemAdmin",
                    CreatedDate = DateTime.UtcNow,
                    ModifiedBy = "SystemAdmin",
                    ModifiedDate = DateTime.UtcNow,
                    Description = "",
                    Key = LanguageKeys.ENGLISH,
                    Name = "English",
                    IconId = _attachedIds["en"],
                },
                new Language
                {
                    Id = viId,
                    CreatedBy = "SystemAdmin",
                    CreatedDate = DateTime.UtcNow,
                    ModifiedBy = "SystemAdmin",
                    ModifiedDate = DateTime.UtcNow,
                    Description = "",
                    Key = LanguageKeys.VIETNAMESE,
                    Name = "Tiếng Việt",
                    IconId = _attachedIds["vi"],
                }
            );
        }
    }
}