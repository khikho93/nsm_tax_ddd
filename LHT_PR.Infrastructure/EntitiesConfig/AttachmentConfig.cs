﻿using LHT_PR.Shared.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using static LHT_PR.Worker.Domain.Constants.AppConstants;

namespace LHT_PR.Infrastructure.EntitiesConfig
{
    internal class AttachmentConfig : IEntityTypeConfiguration<Attachment>
    {
        public Dictionary<string, Guid> AddedIds = new Dictionary<string, Guid>();

        public void Configure(EntityTypeBuilder<Attachment> builder)
        {
            Guid enId = new Guid("fe26ba62-ae5c-4600-9617-3de2433247d5"),
                viId = new Guid("b1a9beb4-00ff-4591-937f-d9d6b389b22f");

            builder
                .HasData(
                    new Attachment()
                    {
                        Id = enId,
                        CreatedBy = "SystemAdmin",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = "SystemAdmin",
                        ModifiedDate = DateTime.UtcNow,
                        Extension = ".png",
                        RealName = "icons8-usa-48.png",
                        Name = "icons8-usa-48.png",
                        Type = "IMAGE/PNG",
                        IsSecure = false,
                    },
                    new Attachment()
                    {
                        Id = viId,
                        CreatedBy = "SystemAdmin",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = "SystemAdmin",
                        ModifiedDate = DateTime.UtcNow,
                        Extension = ".png",
                        RealName = "icons8-vietnam-48.png",
                        Name = "icons8-vietnam-48.png",
                        Type = "IMAGE/PNG",
                        IsSecure = false
                    }
                );

            AddedIds.Add(LanguageKeys.VIETNAMESE, viId);
            AddedIds.Add(LanguageKeys.ENGLISH, enId);
        }
    }
}